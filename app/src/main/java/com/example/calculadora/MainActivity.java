package com.example.calculadora;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button btnsuma;
    private Button btnresta;
    private Button btnmult;
    private Button btndiv;
    private Button btnlimp;
    private Button btnsalir;
    private TextView text_respuesta;
    private EditText editUno;
    private EditText editDos;

    private Calculadora calculadora;


    private void btnSuma(){
        calculadora = new Calculadora(
                Integer.parseInt(editUno.getText().toString()),
                Integer.parseInt(editDos.getText().toString())
        );
        double total = calculadora.suma();
        text_respuesta.setText("" + total);
    }
    private void btnResta(){
        calculadora = new Calculadora(
                Integer.parseInt(editUno.getText().toString()),
                Integer.parseInt(editDos.getText().toString())
        );
        double total = calculadora.resta();
        text_respuesta.setText("" + total);
    }
    private void btnMult(){
        calculadora = new Calculadora(
                Integer.parseInt(editUno.getText().toString()),
                Integer.parseInt(editDos.getText().toString())
        );
        double total = calculadora.multiplicacion();
        text_respuesta.setText("" + total);
    }
    private void btnDiv(){
        calculadora = new Calculadora(
                Integer.parseInt(editUno.getText().toString()),
                Integer.parseInt(editDos.getText().toString())
        );
        double total = calculadora.division();
        text_respuesta.setText("" + total);
    }

    private void btnLimp(){
        text_respuesta.setText("");
        editUno.setText("");
        editDos.setText("");
    }

    private void btnSalir(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("¿CERRAR APP?");
        confirmar.setMessage("Se descartará toda la información");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                aceptar();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        confirmar.show();
    }
    private void aceptar(){
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnsuma = findViewById(R.id.button_suma);
        btnresta = findViewById(R.id.button_resta);
        btnmult = findViewById(R.id.button_multi);
        btndiv = findViewById(R.id.button_divi);
        btnlimp = findViewById(R.id.button_limpiar);
        btnsalir = findViewById(R.id.button_salir);
        text_respuesta = findViewById(R.id.resultado);
        editUno = findViewById(R.id.num1);
        editDos = findViewById(R.id.num2);
        btnsuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSuma();
            }
        });
        btnresta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnResta();
            }
        });
        btnmult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnMult();
            }
        });
        btndiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDiv();
            }
        });
        btnlimp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnLimp();
            }
        });
        btnsalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSalir();
            }
        });
    }

}