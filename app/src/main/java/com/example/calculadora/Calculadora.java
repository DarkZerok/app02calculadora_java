package com.example.calculadora;

public class Calculadora {
    private int num1;
    private int num2;

    public Calculadora(){
        this.num1 = 0;
        this.num2 = 0;
    }

    public Calculadora(int num1, int num2){
        this.num1 = num1;
        this.num2 = num2;
    }
    public Calculadora(Calculadora calculadora){
        this.num1 = calculadora.num1;
        this.num2 = calculadora.num2;
    }
    public int getNum1(){return num1;}
    public void setNum1(int num1){this.num1 = num1;}
    public int getNum2(){return num2;}
    public void setNum2(int num2){this.num2 = num2;}

    public double suma(){
        int var1 = this.num1;
        int var2 = this.num2;
        int total = 0;
        total = var1 + var2;
        return total;
    }
    public double resta(){
        int var1 = this.num1;
        int var2 = this.num2;
        int total = 0;
        total = var1 - var2;
        return total;
    }
    public double multiplicacion(){
        int var1 = this.num1;
        int var2 = this.num2;
        int total = 0;
        total = var1 * var2;
        return total;
    }
    public double division(){
        int var1 = this.num1;
        int var2 = this.num2;
        int total = 0;
        if(var2!=0){
            total = var1/var2;
        }
        return total;
    }

}
